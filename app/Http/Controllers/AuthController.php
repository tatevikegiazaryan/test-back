<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'name'     => 'required|max:20',
            'email'    => 'required|email|unique:users',
            'password' =>'required|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => "Something wrong",
            ],400);
        }

        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response($data,201);
     }


    public function login()
    {
        $credentials = request(['email', 'password']);
        if (empty($credentials['email']) || empty($credentials['password']) ) {
                return response()->json([
                    'message' => 'Fill in the  fields'
                ],400);
        }
        $user = User::where('email', $credentials['email'])->first();

      if ($user && Hash::check($credentials['password'], $user->password)) {
            $token = auth()->attempt($credentials);
              if (!$token) {
                  return response()->json(['error' => 'Unauthorized'], 401);
              }
      } else {
          return response()->json([
              'message' => 'Not match email or password'
          ],400);
      }


        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
