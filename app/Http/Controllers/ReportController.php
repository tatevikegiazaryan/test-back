<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
 
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {   
        if($req->type === null) {
            $data = Report::orderby('id', 'desc')->paginate(5); 
            return response()->json($data,200);

        } else {
            $data = Report::where('type', $req->type)->orderby('id', 'desc')->paginate(5); 
            return response()->json($data, 200);
        }
    }

    public function editingReport(Request $req) {

        $data = Report::find($req->id);
        return response()->json($data, 200);
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $user_id = auth()->id();

         $data = $request->all();
         $validator = validator::make($data, [
           'title' => 'required',
           'type' => 'required',

        ]);

        if($validator->fails()) {
            return response()->json([
                "error" => 'Field is required',
            ],400);
        }

        $report = Report::create([
            'user_id' =>1,
            'title' => $request->title,
            'type' => $request->type,
            'content' => $request->content,

        ]);

        return response($report,201);

    } 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $report = Report::find($req->id)->update([
            'title' => $req->title,
            'content' => $req->content,
            'type' => $req->type,

        ]);

        return response()->json($report,200);
    } 
}
