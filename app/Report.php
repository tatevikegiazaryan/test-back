<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
     protected $fillable = [
        'user_id','type','title','content'
    ];
    protected $hidden = ['created_at','updated_at'];

}
